@AppCtrl = ($scope, $http, $route, $location) ->
	$scope.$location = $location
	
	$http({method: 'GET', url: '/api/name'}).
	success((data, status, headers, config) ->
		$scope.name = data.name;
	).
	error((data, status, headers, config) ->
		$scope.name = 'Error!'
	)