@MenuCtrl = ($scope, $location, Menu) -> 
	Menu.list {}, (data) ->
		$scope.menuList = data.menuItems
	
	$scope.newMenu = new Menu()

	$scope.create = ->
		$scope.newMenu.$save( (data) -> 
			$scope.menuList.push(data.menuItem))