mongoose = require('mongoose');
mongoose.connect(process.env.MONGOLAB_URI || 'mongodb://localhost/db');

usersSchema = mongoose.Schema({title: 'string'});
User = mongoose.model('User', usersSchema);