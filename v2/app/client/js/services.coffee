'use strict'

# Services 


# Demonstrate how to register services
# In this case it is a simple value service.
angular.module('myApp.services', ['ngResource']).
value('version', '0.1')
.factory "Menu",
["$resource",
	($resource) ->

		url = "/services/menu/:id"

		$resource url, {}, {

			list: {method: "GET", params: {menuId: ""}},
			get: {method: "GET", params: {}}

		}
	]