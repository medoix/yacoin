mongoose = require('mongoose')

menu = module.exports = {
list: (req, res) ->
	Menu = mongoose.model('Menu')
	Menu.find {}, (err, menuItems) -> res.json { menuItems: menuItems }
,
get: (req, res) ->
	Menu = mongoose.model('Menu')
	id = req.params.id
	if not id then res.json 400, {error: "Missing menu id"}

	Menu.findOne {_id: id}, (err, menuItems) -> res.json { menuItems: menuItems }
,
create: (req, res) ->
	Menu = mongoose.model('Menu')
	newMenu = new Menu(req.body)
	newMenu.save((err) -> console.log err)
	res.json { menuItem: newMenu }
}