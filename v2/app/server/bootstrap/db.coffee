mongoose = require 'mongoose'
fs = require 'fs'
cfg = require('../config').Config

@connect =  ->
  connection = makeConnection()
  loadSchemas()
  require './bootstrap'
  return connection

makeConnection = ->
  mongo = { "hostname":'localhost', "port":27017, "username":"yacoin", "password":"yacoin", "db":"yacoin" }

  return mongoose.connect(generate_mongo_url(mongo)) 

generate_mongo_url = (obj) ->
  obj.hostname = (obj.hostname || 'localhost');
  obj.port = (obj.port || 27017);
  obj.db = (obj.db || 'db');
  
  if (obj.username && obj.password)
    return "mongodb://" + obj.username + ":" + obj.password + "@" + obj.hostname + ":" + obj.port + "/" + obj.db
  else 
    return "mongodb://" + obj.hostname + ":" + obj.port + "/" + obj.db

loadSchemas = ->
  models_path = fs.realpathSync('app/server/models')
  console.log models_path
  
  fs.readdirSync(models_path).forEach( (file) ->
    require(models_path+'/'+file)
  )
 