###
Serve JSON to our AngularJS client
###
mongoose = require('mongoose')

exports.name = (req, res) ->
	User = mongoose.model('User')	
	User.find {}, (err, user) ->
		if user.length == 0
			res.json name: "Anonymous"	
		else 
			res.json name: user[0].title
