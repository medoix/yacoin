# main home page
exports.index = (req, res) ->
    res.render 'home',
        title : 'YACOIN',
        path : req.path,
        udata : req.user

# about page
exports.about = (req, res) ->
    res.render 'page-about',
        title : 'YACOIN - About',
        path : req.path,
        udata : req.user

# faq page
exports.faq = (req, res) ->
    res.render 'page-faq',
        title : 'YACOIN - FAQ',
        path : req.path,
        udata : req.user

# coins page
exports.coins = (req, res) ->
    coins = app.get('coins')
    res.render 'page-coins',
        title : 'YACOIN - Coins',
        path : req.path,
        udata : req.user,
        coins : coins

# features page
exports.features = (req, res) ->
    res.render 'page-features',
        title : 'YACOIN - Features',
        path : req.path,
        udata : req.user