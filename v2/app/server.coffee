###
Module dependencies.
###

###
Modules
###
passport = require('passport')
express = require('express')
fs = require('fs')
assets = require('connect-assets')

# Settings
cfg = require('./server/config').Config
routes = require('./server/routes')
#api = require('./server/routes/api')
db = require('./server/bootstrap/db')
pass = require('./server/pass')

app = module.exports = express()

# Configuration

app.configure ->
  app.set 'ipaddr', cfg.socketHost
  app.set 'port', cfg.socketPort
  app.set 'coins', cfg.coins
  app.set('views', __dirname + '/server/views');
  app.set 'view engine', 'jade'
  app.use express.logger()
  app.use express.cookieParser()
  app.use express.bodyParser()
  app.use express.methodOverride()

  # use express.session before passport, so that passport session will work
  app.use express.session({ secret: 'keyboard cat' })

  # Initialize Passport!  Also use passport.session() middleware, to support
  # persistent login sessions (recommended).
  app.use passport.initialize()
  app.use passport.session()

  app.use assets(src: __dirname + '/client')
  app.use express.static(__dirname + '/public')
  app.use app.router

app.configure 'development', ->
  app.use express.errorHandler { dumpExceptions: true, showStack: true }

app.configure 'production', ->
  app.use express.errorHandler()

# Routes

# set up our security to be enforced on all requests to secure paths
#app.all '/secure', pass.ensureAuthenticated
#app.all '/secure/admin', pass.ensureAdmin

# normal routes
app.get '/', routes.index
app.get '/about', routes.about
app.get '/faq', routes.faq
app.get '/coins', routes.coins
app.get '/features', routes.features

# login pages
#app.get '/login', routes.getlogin
#app.post '/login', routes.postlogin
#app.get '/logout', routes.logout

# signup pages
#app.get '/signup', routes.getsignup
#app.post '/signup', routes.signup

# secure pages
#app.get '/secure/account', routes.account

# admin pages
#app.get '/secure/admin', routes.admin

# JSON API

#app.get '/api/name', api.name

# Menu API
#menu = require "./server/services/menu"
#app.get '/services/menu/:id', menu.get
#app.get '/services/menu', menu.list
#app.post '/services/menu', menu.create

# redirect all others to the index (HTML5 history)
app.get '*', routes.index

db.connect()

# Start server
app.listen 8080, ->
  console.log "Server started on port 8080"
