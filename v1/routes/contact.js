var EM = require('../modules/email-dispatcher');

module.exports = function(app) {

    // contact page //
    app.get('/contact', function(req, res){
        res.render('contact', {
            title : 'YACOIN - Contact Us',
            path : req.path,
            udata : req.session.user
        });
    });

    // contact page //
    app.post('/contact', function(req, res){
		EM.dispatchContactForm(req, function(e, m){
		// this callback takes a moment to return //
		// should add an ajax loader to give user feedback //
			if (!e) {
				res.send('ok', 200);
			} else {
				res.send('email-server-error' + e, 400);
				for (k in e) console.log('error : ', k, e[k]);
			}
		});
	});
};