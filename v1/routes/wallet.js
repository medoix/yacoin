var cfg = require('../modules/config').Config;

var bitcoin = require('bitcoin');
//var WM = require('../modules/wallet-manager');
var async = require('async');
var string = require('string');
var client;

module.exports = function(app) {

    connect = function(coin)
    {
        client = new bitcoin.Client({
            host: cfg.coinHost,
            port: coin,
            user: cfg.coinUser,
            pass: cfg.coinPass,
        });
    }

    // list all wallets //
    app.get('/wallet', function(req, res){
        if (req.session.user == null){
            // if user is not logged-in redirect back to login page //
            res.redirect('/');
        } else {
            var coins = app.get('coins');
            async.mapSeries(coins, function(coin, cb) {
                
                connect(coin.port);

                client.getBalance(req.session.user._id, 6, function(err, balance) {
                    if (err) balance = "Offline";

                    client.getAddressesByAccount(req.session.user._id, function(err, address) {
                        if (err) address = "Offline";
                    
                        console.log(coin.abr + " address = " + address);
                        console.log(coin.abr + " balance = " + balance);

                        return cb(null, {
                            name: coin.name,
                            display: coin.display,
                            address: address,
                            balance: balance,
                            abr: coin.abr,
                        });
                    });
                });
            }, function(err, coins) {
                console.log(coins);
                if (err) {
                    return console.log(err);
                }
    
                res.render('wallet', {
                    title: 'YACOIN - Wallet',
                    coins: coins,
                    path : req.path,
                    udata : req.session.user
                });
            });
        }
    });
    
    // new wallet address //
    app.get('/new_address/:coin', function(req, res){
        if (req.session.user == null){
            // if user is not logged-in redirect back to login page //
            res.redirect('/');
        } else {
            var coins = app.get('coins');
            async.each(coins, function(coin, cb) {
                if (coin.name == req.params.coin)  {
                    console.log('Coin Match! - ' + coin.name);
                    connect(coin.port);
        
                    client.getNewAddress(req.session.user._id, function(err, address) {
                        if (err) cb(err);
                    });
                    // redirect back to wallet once new address has been created //
                    res.redirect('/wallet');
                }
            }, function(err, coins) {
                if (err) {
                    return console.log(err);
                }
                // If callback called then return home //
                console.log('this message is if async fails i think');
                res.redirect('/wallet');
            });
        }
    });

    // next page //

};