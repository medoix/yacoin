var cfg = require('../modules/config').Config;

module.exports = function(app) {

    // main home page //
    app.get('/', function(req, res){
        res.render('home', {
            title : 'YACOIN',
            path : req.path,
            udata : req.session.user
        });
    });
    
    // about page //
    app.get('/about', function(req, res){
        res.render('page-about', {
            title : 'YACOIN - About',
            path : req.path,
            udata : req.session.user
        });
    });

    // faq page //
    app.get('/faq', function(req, res){
        res.render('page-faq', {
            title : 'YACOIN - FAQ',
            path : req.path,
            udata : req.session.user
        });
    });

    // coins page //
    app.get('/coins', function(req, res){
        var coins = app.get('coins');
        res.render('page-coins', {
            title : 'YACOIN - Coins',
            path : req.path,
            udata : req.session.user,
            coins : coins
        });
    });

    // features page //
    app.get('/features', function(req, res){
        res.render('page-features', {
            title : 'YACOIN - Features',
            path : req.path,
            udata : req.session.user
        });
    });

    // next page //
    
};
