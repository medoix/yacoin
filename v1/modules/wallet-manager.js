var cfg = require('./config').Config;

var bitcoin = require('bitcoin');

exports.connect = function(coin)
{
    return client = new bitcoin.Client({
        host: cfg.coinHost,
        port: coin,
        user: cfg.coinUser,
        pass: cfg.coinPass,
    });
}