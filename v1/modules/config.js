var development = {
  // Web Server Settings
  socketPort : 8080,
  socketHost : '127.0.0.1',
  // Database Settings
  dbHost : '127.0.0.1',
  dbPort : '27017',
  dbName : 'yacoin',
  dbUser : 'yacoin',
  dbPass : 'yacoin',
  // Wallet Settings
  coinHost : 'localhost',
  coinUser : 'yacoin',
  coinPass : 'rpcyacoinpassword',
  coins : [
    {
      name: 'bitcoin',
      display: 'Bitcoin',
      abr: 'BTC',
      desc: 'Bitcoin is a digital currency.',
      url: 'http://www.bitcoin.org',
      img: '/img/bitcoin.png',
      port: 8332
    },
    {
      name: 'litecoin',
      display: 'Litecoin',
      abr: 'LTC',
      desc: 'Litecoin is a digital currency.',
      url: 'http://www.litecoin.org',
      img: '/img/litecoin.png',
      port: 8331
    },
  ],
  // Environement Setting
  env : global.process.env.NODE_ENV || 'development'
};

var production = {
  // Web Server Settings
  socketPort : 8080,
  socketHost : '127.0.0.1',
  // Database Settings
  dbHost : 'dharma.mongohq.com',
  dbPort : '10072',
  dbName : 'yacoin-dev',
  dbUser : 'yacoin-dev',
  dbPass : 'MongoDBPassword!',
  // Wallet Settings
  coinHost : '127.0.0.1',
  coinUser : 'yacoin',
  coinPass : 'rpcyacoinpassword',
	coins : [
    {
      name: 'bitcoin',
      display: 'Bitcoin',
      abr: 'BTC',
      desc: 'Bitcoin is a digital currency.',
      url: 'http://www.bitcoin.org',
      img: '/img/bitcoin.png',
      port: 8332
    },
    {
      name: 'litecoin',
      display: 'Litecoin',
      abr: 'LTC',
      desc: 'Litecoin is a digital currency.',
      url: 'http://www.litecoin.org',
      img: '/img/litecoin.png',
      port: 8331
    },
    {
      name: 'namecoin',
      display: 'Namecoin',
      abr: 'NMC',
      desc: 'Namecoin is a digital currency.',
      url: 'http://www.namecoin.info',
      img: '/img/namecoin.png',
      port: 8330
    },
    {
      name: 'devcoin',
      display: 'Devcoin',
      abr: 'DVC',
      desc: 'Devcoin is a digital currency.',
      url: 'http://www.devcoin.org',
      img: '/img/devcoin.png',
      port: 8329
    },
    {
      name: 'ixcoin',
      display: 'IXCoin',
      abr: 'IXC',
      desc: 'IXCoin is a digital currency.',
      url: 'http://www.ixcoin.org',
      img: '/img/ixcoin.png',
      port: 8328
    },
    {
      name: 'feathercoin',
      display: 'Feathercoin',
      abr: 'FTC',
      desc: 'Feathercoin is a digital currency.',
      url: 'http://www.feathercoin.com',
      img: '/img/feathercoin.png',
      port: 8327
    },
    {
      name: 'bbqcoin',
      display: 'BBQCoin',
      abr: 'BQC',
      desc: 'BBQCoin is a digital currency.',
      url: 'http://www.bbqcoin.org',
      img: '/img/bbqcoin.png',
      port: 8326
    },
    {
      name: 'yacoin',
      display: 'YACoin',
      abr: 'YAC',
      desc: 'YACoin is a digital currency.',
      url: 'http://www.yacoin.org',
      img: '/img/yacoin.png',
      port: 8325
    },
    {
      name: 'terracoin',
      display: 'Terracoin',
      abr: 'TRC',
      desc: 'Terracoin is a digital currency.',
      url: 'http://www.terracoin.org',
      img: '/img/terracoin.png',
      port: 8324
    },
    {
      name: 'primecoin',
      display: 'Primecoin',
      abr: 'XPM',
      desc: 'Primecoin is a digital currency.',
      url: 'http://www.primecoin.org',
      img: '/img/primecoin.png',
      port: 8323
    },
  ],
  env : global.process.env.NODE_ENV || 'development'
};

exports.Config = global.process.env.NODE_ENV === 'development' ? development : production;
