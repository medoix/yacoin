var ES = require('./email-settings');
var EM = {};
module.exports = EM;

if(ES.type == 'sendmail') {
	console.log('Using SENDMAIL');
	EM.server = require("nodemailer").createTransport("Sendmail", "/usr/sbin/sendmail");
};
if(ES.type == 'SMTP') {
	console.log('Using SMTP');
	EM.server = require("nodemailer").createTransport("SMTP", {
		host: ES.host,
		port: ES.port,
		secureConnection: true,
		debug: true,
		auth: {
			user: ES.user,
			pass: ES.password,
		}
	});
};

// PASSWORD RESET EMAIL //
EM.dispatchResetPasswordLink = function(account, callback)
{
	var link = 'http://yacoin.com/reset-password?e='+account.email+'&p='+account.pass;
	var html = "<html><body>";
		html += "Hi "+account.name+",<br><br>";
		html += "Your username is: <b>"+account.user+"</b><br><br>";
		html += "<a href='"+link+"'>Please click here to reset your password</a><br><br>";
		html += "Cheers,<br>";
		html += "</body></html>";

	EM.server.send({
		from : ES.sender,
		to : account.email,
		subject : 'Password Reset',
		html : html
	}, callback );
}

// CONTACT FORM EMAIL //
EM.dispatchContactForm = function(details, callback)
{
	var html = "<html><body>";
		html += "------- User --------<br>";
		html += details.body.name+"<br><br>";
		html += "------- EMail -------<br>";
		html += details.body.email+"<br><br>";
		html += "------ Message ------<br>";
		html += details.body.message+"<br><br>";
		html += "</body></html>";

	EM.server.sendMail({
		from : ES.sender,
		to : 'medoix@gmail.com',
		subject : details.body.subject,
		html : html
	}, callback );
}