var cfg = require('./modules/config').Config;

var express = require('express')
  , http = require('http')
  //, https = require('https')
  , fs = require('fs')
  //, keys_dir = 'keys/'
  //, server_options = {
  //      key : fs.readFileSync(keys_dir + 'privatekey.pem'),
  //      ca : fs.readFileSync(keys_dir + 'certrequest.csr'),
  //      cert : fs.readFileSync(keys_dir + 'certificate.pem')
  //  }
  , app = express();

// Configure express
app.configure(function() {
    app.set('ipaddr', cfg.socketHost);
    app.set('port', cfg.socketPort);
    app.set('coins', cfg.coins);
    app.set('views', __dirname + '/views');
    app.set('view engine', 'jade');
    app.use(express.favicon());
    app.use(express.logger('dev'));
    app.use(express.cookieParser());
    app.use(express.session({
        secret: 'gfhjgfj65jke7kjyrtheatjaq6'
        })
    );
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    //app.use(require('stylus').middleware({ src: __dirname + 'public' }));
    app.use(express.static(__dirname + '/public'));
    app.use(app.router);
});

require('./routes')(app);

// HTTP Server
http.createServer(app).listen(app.get('port'), app.get('ipaddr'), function(){
  console.log('HTTP server listening on port ' + app.get('port'));
});

// HTTPS Server
//https.createServer(server_options,app).listen(app.get('httpsport'), app.get('ipaddr'), function(){
//  console.log('HTTPS server listening on port ' + app.get('port'));
//});
